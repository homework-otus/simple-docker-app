```shell script
sudo docker run -p=5432=5432 --name PG -e POSTGRES_PASSWORD=postgres -d postgres:12.2-alpine
```

```postgresql
CREATE TABLE app_users (
    id UUID,
    nickname varchar(32),
    phone varchar(32)
);
```