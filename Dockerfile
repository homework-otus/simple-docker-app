FROM maven:3.6.3-jdk-14 as mavenBuilder
WORKDIR /build/
COPY . .
RUN mvn package --batch-mode

FROM openjdk:14-alpine
LABEL maintainer="toliyansky" \
      version="1.4"
WORKDIR /var/lib/simpler-docker-app/
COPY --from=mavenBuilder /build/target/simple-docker-app.jar .
# TODO: change to specific user (not root)
EXPOSE 8000
CMD ["sh", "-c", "java -jar simple-docker-app.jar"]
