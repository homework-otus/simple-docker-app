package simple.converter;

import org.springframework.stereotype.Component;
import simple.dto.UserDto;
import simple.entity.UserEntity;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserConverter {

    public List<UserEntity> toEntity(List<UserDto> userDtoList) {
        return userDtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    public List<UserDto> toDto(List<UserEntity> userEntities) {
        return userEntities.stream().map(this::toDto).collect(Collectors.toList());
    }

    public UserEntity toEntity(UserDto userDto) {
        return UserEntity.builder()
                .id(userDto.getId())
                .nickname(userDto.getNickname())
                .phone(userDto.getPhone())
                .build();
    }

    public UserDto toDto(UserEntity userEntity) {
        return UserDto.builder()
                .id(userEntity.getId())
                .nickname(userEntity.getNickname())
                .phone(userEntity.getPhone())
                .build();
    }
}
