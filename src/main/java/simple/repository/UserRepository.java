package simple.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import simple.entity.UserEntity;

import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, UUID> {
}
