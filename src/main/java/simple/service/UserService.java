package simple.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import simple.converter.UserConverter;
import simple.dto.UserDto;
import simple.repository.UserRepository;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserConverter userConverter;
    private final UserRepository userRepository;

    public List<UserDto> getAll() {
        var userEntities = userRepository.findAll();
        return userConverter.toDto(userEntities);
    }

    public UserDto getById(UUID id) {
        var userEntityOptional = userRepository.findById(id);
        return userEntityOptional.map(userConverter::toDto).orElse(null);
    }

    public UserDto createUser(UserDto userDto) {
        var userEntity = userConverter.toEntity(userDto);
        userEntity.setId(UUID.randomUUID());
        userEntity = userRepository.save(userEntity);
        return userConverter.toDto(userEntity);
    }

    public UserDto updateUser(UserDto userDto) {
        var userEntity = userConverter.toEntity(userDto);
        userEntity = userRepository.save(userEntity);
        return userConverter.toDto(userEntity);
    }

    public boolean deleteUser(UUID id) {
        var userEntityOptional = userRepository.findById(id);
        if (userEntityOptional.isPresent()) {
            userRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
