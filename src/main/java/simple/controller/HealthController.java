package simple.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.management.ManagementFactory;
import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("/")
public class HealthController {

    @Value("${spring.application.instance-id}")
    private UUID instanceId;

    /**
     * Readness probe route. Need for root "/"
     */
    @GetMapping
    public String readnessProbe() {
        return "";
    }

    /**
     * Liveness probe.
     *
     * @return status OK if service ready to work, else status.
     */
    @GetMapping("/health")
    public CustomResponse livenessProbe() {
        log.info("Heath check request at {}", Instant.now());
        var upTime = ManagementFactory.getRuntimeMXBean().getUptime();
        // Imitate delay for ready to work. For example health controller init, but smth job required for work not end.
        var isAppReadyToWork = upTime > TimeUnit.SECONDS.toMillis(20);
        return CustomResponse.builder()
                .instanceId(instanceId)
                .status(isAppReadyToWork ? Status.OK : Status.SERVICE_UNAVAILABLE)
                .build();
    }
}
