package simple.controller;

public enum Status {
    OK,
    SERVICE_UNAVAILABLE
}
