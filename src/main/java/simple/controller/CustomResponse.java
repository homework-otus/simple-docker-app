package simple.controller;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class CustomResponse {
    private UUID instanceId;
    private Status status;
}